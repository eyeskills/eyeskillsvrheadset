//TODO: Can we make headsetWidth responsive to pd?  At least 18cm wide, but always 10cm wider than PD?
headsetWidth=18; 
headsetHeight=12;
headsetDepth=10;
headsetThickness=0.4;
pd=8;
lensRadius=2;
cameraBarHeight=4;
IREmitterHoleRadius=0.4;
CAMHoleRadius=0.6;
dioptrineLeft = 0;
dioptrineRight = 0;
lensPositionLowering=-1;
plateauForPhoneToSitOn=3;
headLength=18;
headHeight=22;
headWidthRadius=9;
distanceOfForeheadFromCenterOfHeadset=3;

//Make a series of modules which are layed flat 
//Clone them to show the 3D model in various stages of build

//would be awesome if we could call something like addTabs 
//and it operates on the child to place tabs
module tab(){
    cube([2,1,headsetThickness],center=true);
}

module longTab(){
    cube([4,1,headsetThickness],center=true);
}

module lensHolderLeft(){    
    union(){
    
        difference(){ 
            cube([headsetWidth/2,headsetHeight, headsetThickness],center=true);       
            translate([headsetWidth/4-pd/2,lensPositionLowering,0]){     
                cylinder(headsetThickness + 1,lensRadius,lensRadius,center=true,$fn=50);
            }            
        }
        
        for (i=[headsetWidth/8,-headsetWidth/8]){
            for (y=[headsetHeight/2,-headsetHeight/2]){
                translate([i,y,0]){
                    tab();
                }
            }
        }            
        
    }   
}

module lensHolderRight(){
    mirror(0,1,0){
        lensHolderLeft();
    }
}


module lensPair(){
    translate([-headsetWidth/4,0,dioptrineLeft]){
        lensHolderLeft();
    }
    translate([headsetWidth/4,0,dioptrineRight]){
        lensHolderRight();
    }
}

module roofPlate(){
    difference(){
        union(){
            difference(){
                translate([0,headsetHeight/2,0]){
                        cube([headsetWidth,headsetThickness, headsetDepth],center=true);       
                }
                lensPair();        
            } 
            
             //short tabs
             for (i=[1,-1]){
                 for (y=[1,-1]){
                    translate([i*headsetWidth/2+headsetThickness/2,headsetHeight/2,y*headsetDepth/2-y-y]){
                        rotate(a=[90,0,0]){            
                            tab();
                        }
                    }
                }
            }
        }
    head();
    }
}

//The phone needs to sit on the bottom plate, while the front facing unit holds it in position and provides cam input.

module bottomPlate(){
    //Particularly large tabs for bottom plate at back to hook rubber band around
    difference(){
        union(){
            difference(){
                translate([0,-headsetHeight/2,+plateauForPhoneToSitOn/2]){
                        cube([headsetWidth,headsetThickness, headsetDepth+plateauForPhoneToSitOn],center=true);       
                }
                lensPair();        
            } 
            //Left/Right long tabs for rubber band - also going around both camera panels
            for (i=[1,-1]){
                translate([i*headsetWidth/2+i+headsetThickness/2,-headsetHeight/2,-headsetDepth/2+2]){
                    rotate(a=[90,0,0]){            
                        longTab();
                    }
                }
            }

            //short tabs
             for (i=[1,-1]){
             translate([i*headsetWidth/2 + headsetThickness/2,-headsetHeight/2,headsetDepth/2-2]){
                    rotate(a=[90,0,0]){            
                        tab();
                    }
                }
            }
        }
        head();
    }
}

module cameraPlate(){
    rotate(a=[-10,0,0]){
        translate([0,headsetHeight/2-cameraBarHeight/2-headsetThickness,-1]){
            difference(){
                cube([headsetWidth+6,cameraBarHeight,headsetThickness],center=true);
                //hole for IR sensor
                cylinder(headsetThickness+1,IREmitterHoleRadius,IREmitterHoleRadius,center=true,$fn=50);
                //hole for camera - hotglue ESP32 on cam bar either side of cam holes
                for (i=[-1,1]){
                  translate([i*pd/2,0,0]){
                    cylinder(headsetThickness+1,CAMHoleRadius,IREmitterHoleRadius,center=true,$fn=50);                
                  }
                  //fixings for headstrap?
                  translate([i*(headsetWidth+1.5)/2,0,0]){
                    cube([0.5,2,headsetThickness*2],center=true);               
                    cube([0.5,2,headsetThickness*2],center=true);               
                  }
                  translate([i*(headsetWidth+3.5)/2,0,0]){
                    cube([0.5,2,headsetThickness*2],center=true);               
                    cube([0.5,2,headsetThickness*2],center=true);               
                  }
                } 
            }
        }
    }
}

module cameraUnit(){
    cube([4,1.5,0.8],center=true,color=black);
}

module frontCamPlate(){
    rotate(a=[0,0,0]){
        translate([0,lensPositionLowering,headsetDepth]){         
            //hole for camera - hotglue ESP32 on cam bar either side of cam holes
            for (i=[-1,1]){
              translate([i*pd/2,0,0]){
                color([1,0,0]) cameraUnit();                
              }               
            } 
            difference(){
                color([0,1,0]) cube([headsetWidth+6,cameraBarHeight,headsetThickness],center=true);
                for (i=[1,-1]){
                    for (y=[1,-1]){
                        translate([i*headsetWidth/2+i*headsetThickness,y*cameraBarHeight/2-y*headsetThickness/2+y*0.01,0]){
                            rotate(a=[0,90,90]){
                               tab();
                            }
                        }
                    }                    
                }
            }
        }
    }
}

module sidePlateLeft(){
    
    difference(){
        translate([headsetWidth/2+headsetThickness/2,0,0]){
            cube([headsetThickness,headsetHeight+2,headsetDepth],center=true);
        }
        roofPlate();
        bottomPlate();
        cameraPlate();
    }
}

module sidePlateRight(){
    
    difference(){
        translate([-headsetWidth/2+headsetThickness/2,0,0]){
            cube([headsetThickness,headsetHeight+2,headsetDepth],center=true);
        }
        roofPlate();
        bottomPlate();
        cameraPlate();
    }

}

module head(){
    
    rotate([10,0,0])
        union(){
            translate([0,0,-headLength/2-distanceOfForeheadFromCenterOfHeadset+5]){
                color([0,0,1,0.5])
                rotate([65,0,0])
                cylinder(10,3,3,$fn=100);
            }
            
            translate([0,headHeight/2,-headLength/2-distanceOfForeheadFromCenterOfHeadset]){
                color([0,0,1,0.5])
                scale([(headWidthRadius*2)/headLength,1,1])
                rotate([90,0,0])
                cylinder(headHeight,headWidthRadius,headWidthRadius,$fn=100);
            }
        }
    
}

lensPair();
cameraPlate();
roofPlate();
bottomPlate();
sidePlateLeft();
sidePlateRight();
frontCamPlate();

//head();

//Add roof plate. Intersect. See if we can then replicate it.
//Add bar in front of lens plates to hold cameras and IR with output for battery - simpler to assemble and is then a fixed distance from the eyes!
//Add wording to the plates

//Try to dissassemble
//Try to add a t value for animating the reconstruction of
//the parts